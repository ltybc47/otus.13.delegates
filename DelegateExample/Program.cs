﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DelegateExample
{
  class Program
  {
    
    static void Main(string[] args)
    {
      Console.WriteLine("\n Задача №1 - вывести максимальное значение кастомнго поля объекта из списка объектов через делегат \n");
      Task1();
      Console.ReadKey();
      Console.WriteLine("\n Задача №2 - обойти каталог файлов и выдать событие при нахождении каждого файла \n");
      Task2();
      Console.ReadKey();
      Console.WriteLine("\n Задача №3 - обойти каталог файлов и прервать обход каталога по условию \n");
      Task3();
      Console.ReadKey();
    }


    ///---------------------------------Task1
    private static void Task1()
    {
      Random r = new Random();
      List<Box> boxes = new List<Box>();

      for (int i = 0; i < 10; i++)
        boxes.Add(new Box(r.Next(1, 10)));
      for (int i = 0; i < 10; i++)
        boxes.Add(new Box(r.Next(1, 10), r.Next(1, 10), r.Next(1, 10)));

      Console.WriteLine("Search Max V : " + boxes.GetMax<Box>(ByV).ToString());
      Console.WriteLine("Search Max LenX : " + boxes.GetMax<Box>(ByLenX).ToString());
      Console.WriteLine("Search Max LenZ : " + boxes.GetMax<Box>((e) => e.LenZ).ToString());
      Console.WriteLine();
      Console.WriteLine(string.Join("\n", boxes.Select(x => x.ToString())));
    }

    private static float ByLenX(Box arg)
    {
      return arg.LenX;
    }

    private static float ByV(Box arg)
    {
      return arg.V;
    }

    ///---------------------------------Task2
    public static Dictionary<string, int> DictTypeCount = new Dictionary<string, int>();
    private static void Task2()
    {
      FileAnalizer fileAnalizer = new FileAnalizer();
      fileAnalizer.FileTypeAnalizer += AnalisCountFilesByType;

      string dir = @"c:\Users\" + Environment.UserName + @"\Downloads\";
      fileAnalizer.CheckDirectory(dir);
      Console.WriteLine(string.Join("\n", DictTypeCount.Select(x => x.Key + " " + x.Value)));

      //не забываем отписываться
      fileAnalizer.FileTypeAnalizer -= AnalisCountFilesByType;

    }

    private static void AnalisCountFilesByType(string fileName)
    {
      string fileType = fileName.Split(".").Last();
      if (DictTypeCount.ContainsKey(fileType))
      {
        DictTypeCount[fileType]++;
      }
      else
      {
        DictTypeCount.Add(fileType, 1);
      }
    }

    ///---------------------------------Task3
    private static void Task3()
    {
      FileAnalizer fileAnalizer = new FileAnalizer();
      fileAnalizer.FileFound += AnaliSizeFile;

      string dir = @"c:\Users\" + Environment.UserName + @"\Downloads\";
      fileAnalizer.CheckDirectory(dir);

      fileAnalizer.FileFound -= AnaliSizeFile;
    }

    private static void AnaliSizeFile(object sender, FileArgs e)
    {
      //какая то логика
      Console.WriteLine(e.FileName);
      if (e.FileName != "password.txt")
      {
        // Через sender обращаемся к классу к которому подписанны и выключаем его работу TASK4
        (sender as FileAnalizer).FlagWork = false;
      }
    }
  }
}
