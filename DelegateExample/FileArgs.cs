﻿using System;

namespace DelegateExample
{
  public class FileArgs : EventArgs
  {
    public string FileName { get; set; }
  }
}