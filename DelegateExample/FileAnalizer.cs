﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace DelegateExample
{
  class FileAnalizer
  {
    public event Action<string> FileTypeAnalizer;
    public event EventHandler<FileArgs> FileFound;
    public bool FlagWork { get; set; }

    public FileAnalizer()
    {
      FlagWork = true;
    }
    public void CheckDirectory( string path)
    {
      if (Directory.Exists(path))
      {
        foreach (var item in Directory.GetFiles(path))
        {
          FileTypeAnalizer?.Invoke(item.Split(@"\").Last());
          FileFound?.Invoke(this, new FileArgs() { FileName = item.Split(@"\").Last() });
          if (FlagWork == false)
          {
            Console.WriteLine("Работа метода прервана одним из подписчиков");
            break;
          }
        }
      }
    }
  }
}
