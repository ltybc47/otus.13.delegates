﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateExample
{
  public static class ExtentionLinq
  {

    public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
    {
      return e.OrderBy(x => getParametr(x)).ToList().Last<T>();
    }
  }
}
