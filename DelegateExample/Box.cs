﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DelegateExample
{
  [DisplayName("Коробка")]
  class Box
  {
    public static int Count { get; set; }
    public int Id { get; set; }
    public int LenX { get; set; }
    public int LenY { get; set; }
    public int LenZ { get; set; }
    public int P { get; set; } = 1000;
    public int V { get { return LenX * LenY * LenZ; } }
    public float Weight { get { return V * P / 1000; } }

    public Box(int CubeSize)
    {
      LenX = CubeSize;
      LenY = CubeSize;
      LenZ = CubeSize;
      Id = Count++;
      
    }
    public Box(int x, int y, int z)
    {
      LenX = x;
      LenY = y;
      LenZ = z;
      Id = Count++;
    }
    public override string ToString()
    {
      return $"Id - {Id}; LenX = {LenX}; LenX = {LenY}; LenZ = {LenZ}; V - {V}; P - {P}; Weight - {Weight}";
    }


  }
}
